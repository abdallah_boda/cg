﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CGUtilities;

namespace CGUtilitiesUnitTest
{
    /// <summary>
    /// Unit Test for Point Utility
    /// </summary>
    [TestClass]
    public class PointUtilTest
    {
        [TestMethod]
        public void CheckCrossProduct()
        {
            Point a = new Point(3, 5);
            Point b = new Point(4, 12);

            Assert.AreEqual(16, a.CrossProduct(b));
        }
        #region In Triangle Tests
        [TestMethod]
        public void InsideTriangleClockwise()
        {
            Point a = new Point(2, 2);
            Point b = new Point(4, 5);
            Point c = new Point(6, 1);

            Point p = new Point(3, 3);
            var res = p.InTriangle(a, b, c);
            Assert.AreEqual(res, Enums.PointInPolygon.Inside);
        }
        [TestMethod]
        public void InsideTriangleCounterClockwise()
        {
            Point a = new Point(6, 1);
            Point b = new Point(4, 5);
            Point c = new Point(2, 2);

            Point p = new Point(3, 3);
            var res = p.InTriangle(a, b, c);
            Assert.AreEqual(res, Enums.PointInPolygon.Inside);
        }
        [TestMethod]
        public void OnTriangleBorder()
        {
            Point a = new Point(3, 4);
            Point b = new Point(6, 6);
            Point c = new Point(3, 8);

            Point p = new Point(3, 6);
            var res = p.InTriangle(a, b, c);
            Assert.AreEqual(res, Enums.PointInPolygon.OnEdge);
        }
        [TestMethod]
        public void OnBorderIfOnHead()
        {
            Point a = new Point(3, 4);
            Point b = new Point(6, 6);
            Point c = new Point(3, 8);

            Point p = new Point(3, 4);
            var res = p.InTriangle(a, b, c);
            Assert.AreEqual(res, Enums.PointInPolygon.OnEdge);
        }
        [TestMethod]
        public void OutsideTriangle()
        {
            Point a = new Point(3, 4);
            Point b = new Point(6, 6);
            Point c = new Point(3, 8);

            Point p = new Point(1, 1);
            var res = p.InTriangle(a, b, c);
            Assert.AreEqual(res, Enums.PointInPolygon.Outside);
        }

        [TestMethod]
        public void OnBorderIfOnBorderAndTriangleIsSegment()
        {
            Point a = new Point(1, 1);
            Point b = new Point(3, 3);
            Point c = new Point(3, 3);

            Point p = new Point(2, 2);
            var res = p.InTriangle(a, b, c);
            Assert.AreEqual(res, Enums.PointInPolygon.OnEdge);
        }
        [TestMethod]
        public void OutsideIfTriangleIsSegmentAndPointIsCollinearAfterEnd()
        {
            Point a = new Point(1, 1);
            Point b = new Point(3, 3);
            Point c = new Point(3, 3);

            Point p = new Point(4, 4);
            var res = p.InTriangle(a, b, c);
            Assert.AreEqual(res, Enums.PointInPolygon.Outside);
        }
        [TestMethod]
        public void OutsideIfTriangleIsSegmentAndPointIsCollinearBeforeStart()
        {
            Point a = new Point(1, 1);
            Point b = new Point(3, 3);
            Point c = new Point(3, 3);

            Point p = new Point(0, 0);
            var res = p.InTriangle(a, b, c);
            Assert.AreEqual(res, Enums.PointInPolygon.Outside);
        }
        [TestMethod]
        public void OnBorderIfTriangleIsThePoint()
        {
            Point a = new Point(3, 3);
            Point b = new Point(3, 3);
            Point c = new Point(3, 3);

            Point p = new Point(3, 3);
            var res = p.InTriangle(a, b, c);
            Assert.AreEqual(res, Enums.PointInPolygon.OnEdge);
        }
        [TestMethod]
        public void OutsideIfTriangleIsPointNotEqualThePoint()
        {
            Point a = new Point(3, 3);
            Point b = new Point(3, 3);
            Point c = new Point(3, 3);

            Point p = new Point(2, 1);
            var res = p.InTriangle(a, b, c);
            Assert.AreEqual(res, Enums.PointInPolygon.Outside);
        }
        #endregion
    }
}
