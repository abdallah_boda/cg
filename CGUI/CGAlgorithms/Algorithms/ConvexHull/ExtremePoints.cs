﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class ExtremePoints : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, ref List<Point> outPoints, ref List<Line> outLines)
        {

        }

        public override string ToString()
        {
            return "Convex Hull - Extreme Points";
        }
    }
}
