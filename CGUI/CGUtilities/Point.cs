﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGUtilities
{
    /// <summary>
    /// The primary Point structure to be used in the CG project.
    /// </summary>
    public struct Point
    {
        /// <summary>
        /// Creates a point structure with the given coordinates.
        /// </summary>
        /// <param name="x">The X value/</param>
        /// <param name="y">The Y value.</param>
        public Point(double x, double y)
            :this()
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Gets or sets the X coordinate.
        /// </summary>
        public double X
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Y coordinate.
        /// </summary>
        public double Y
        {
            get;
            set;
        }
    }
}
