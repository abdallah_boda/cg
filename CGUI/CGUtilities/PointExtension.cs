﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGUtilities
{
    public static class PointExtension
    {
        public static double CrossProduct(this Point a, Point b)
        {
            return a.X * b.Y - a.Y * b.X;
        }
        public static Enums.TurnType CheckTurn(this Point vector1, Point vector2)
        {
            double result = vector1.CrossProduct(vector2);
            if (result < 0) return Enums.TurnType.Right;
            else if (result > 0) return Enums.TurnType.Left;
            else return Enums.TurnType.Colinear;
        }
        public static Point Vector(this Point a, Point to)
        {
            return new Point(to.X - a.X, to.Y - a.Y);
        }
        public static Enums.PointInPolygon InTriangle(this Point p, Point a, Point b, Point c)
        {
            Line ab = new Line(a, b);
            Line bc = new Line(b, c);
            Line ca = new Line(c, a);

            if (
                ab.CheckTurn(p) == Enums.TurnType.Colinear ||
                bc.CheckTurn(p) == Enums.TurnType.Colinear ||
                ca.CheckTurn(p) == Enums.TurnType.Colinear)
                return Enums.PointInPolygon.OnEdge;

            if (ab.CheckTurn(p) == bc.CheckTurn(p) && bc.CheckTurn(p) == ca.CheckTurn(p))
                return Enums.PointInPolygon.Inside;
            return Enums.PointInPolygon.Outside;

        }
    }
}
