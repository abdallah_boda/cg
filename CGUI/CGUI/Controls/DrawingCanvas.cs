﻿using CGUI;
using CGUtilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace CGUI.Controls
{
    /// <summary>
    /// The drawing cavase is used to draw points and lines.
    /// </summary>
    class DrawingCanvas : ContentControl
    {
        /// <summary>
        /// The animation duration to use in all animations in the canvas.
        /// </summary>
        private const int animationDuration = 700;
        
        /// <summary>
        /// The scale factor to use in the scale animation.
        /// </summary>
        private const double scaleAnimationScale = 5;

        /// <summary>
        /// The radius of the point ellipse.
        /// </summary>
        private const double pointRadius = 15;

        /// <summary>
        /// The points collection.
        /// </summary>
        private readonly ObservableCollection<CGUtilities.Point> points;

        /// <summary>
        /// The lines collection.
        /// </summary>
        private readonly ObservableCollection<Line> lines;

        /// <summary>
        /// The main grid, which will be places in the content of the control.
        /// </summary>
        private readonly Grid mainGrid;

        /// <summary>
        /// The grid that holds animation objects only.
        /// </summary>
        private readonly Grid animationGrid;

        /// <summary>
        /// The points grid.
        /// </summary>
        private readonly Grid pointsGrid;

        /// <summary>
        /// The lines grid.
        /// </summary>
        private readonly Grid linesGrid;

        /// <summary>
        /// The line selection grid. Used to guide line creation.
        /// </summary>
        private readonly Grid selectionGrid;

        /// <summary>
        /// The line to use in line creation.
        /// </summary>
        private readonly System.Windows.Shapes.Line selectionLine;

        /// <summary>
        /// Indicates whether the mouse is currently down.
        /// </summary>
        private bool isDragging;

        /// <summary>
        /// The start point of the mouse drag.
        /// </summary>
        private CGUtilities.Point dragStartPoint;

        /// <summary>
        /// The end point of the mouse drag.
        /// </summary>
        private CGUtilities.Point dragEndPoint;

        /// <summary>
        /// Initializes a new instance of the DrawingCanvas class.
        /// </summary>
        public DrawingCanvas()
        {
            // Set the control content.
            this.Content = this.mainGrid = new Grid();

            // Background must not be null (null backgrounds don't pass mouse hit tests).
            this.mainGrid.Background = new SolidColorBrush(Colors.Transparent);
            // Hook the mouse events.
            this.mainGrid.MouseLeftButtonDown += mainGrid_MouseLeftButtonDown;
            this.mainGrid.MouseLeftButtonUp += mainGrid_MouseLeftButtonUp;
            this.mainGrid.MouseMove += mainGrid_MouseMove;
            
            // Init all grids.
            this.mainGrid.Children.Add(this.animationGrid = new Grid());
            this.mainGrid.Children.Add(this.linesGrid = new Grid());
            this.mainGrid.Children.Add(this.pointsGrid = new Grid());
            this.mainGrid.Children.Add(this.selectionGrid = new Grid());

            // Init the selection line.
            this.selectionGrid.Children.Add(this.selectionLine = new System.Windows.Shapes.Line());
            this.selectionLine.Stroke = new SolidColorBrush(Colors.Gray);
            this.selectionLine.StrokeDashArray = new DoubleCollection(new double[] {2,1});

            // Initialized the observable collections (read more about observables).
            this.points = new ObservableCollection<CGUtilities.Point>();
            this.points.CollectionChanged += points_CollectionChanged;
            this.lines = new ObservableCollection<Line>();
            this.lines.CollectionChanged += lines_CollectionChanged;
        }

        /// <summary>
        /// Gets or sets a collection of points to display in the canvas.
        /// </summary>
        public Collection<CGUtilities.Point> Points
        {
            get { return this.points; }
            set
            {
                this.points.Clear();
                foreach (var item in value)
                {
                    this.points.Add(item);
                }
            }
        }

        /// <summary>
        /// Gets or sets a collection of lines to display in the canvas.
        /// </summary>
        public Collection<Line> Lines
        {
            get { return this.lines; }
            set
            {
                this.lines.Clear();
                foreach (var item in value)
                {
                    this.lines.Add(item);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the canvas can be edited or for view only.
        /// </summary>
        public bool IsViewOnly
        {
            get;
            set;
        }

        /// <summary>
        /// Handles the change event in the lines collection.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">Collection change event arguments.</param>
        private void lines_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.HandleCollectionChange(this.lines, e, this.linesGrid);
        }

        /// <summary>
        /// Handles the change event in the points collection.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">Collection change event arguments.</param>
        private void points_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.HandleCollectionChange(this.points, e, this.pointsGrid);
        }

        /// <summary>
        /// Handles the collection change, and updates the UI accordingly.
        /// </summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="e">Collection change event arguments.</param>
        /// <param name="viewGrid">The grid to update.</param>
        private void HandleCollectionChange<T>(ObservableCollection<T> collection, NotifyCollectionChangedEventArgs e, Grid viewGrid)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (typeof(T) == typeof(CGUtilities.Point))
                    {
                        foreach (var item in e.NewItems)
                        {
                            viewGrid.Children.Add(this.CreateElement((CGUtilities.Point)item));
                            this.AddAnimationElement((CGUtilities.Point)item);
                        }
                    }

                    if (typeof(T) == typeof(Line))
                    {
                        foreach (var item in e.NewItems)
                        {
                            viewGrid.Children.Add(this.CreateElement((Line)item));
                            this.AddAnimationElement((Line)item);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    throw new NotSupportedException();
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in e.OldItems)
                    {
                        viewGrid.Children.RemoveAt(e.OldStartingIndex);
                    }
                    break;
                case NotifyCollectionChangedAction.Replace:
                    throw new NotSupportedException();
                case NotifyCollectionChangedAction.Reset:
                    viewGrid.Children.Clear();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Creates a UI element that represents a point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Point UI element.</returns>
        private FrameworkElement CreateElement(CGUtilities.Point point)
        {
            var element = new System.Windows.Shapes.Ellipse()
            {
                Width = pointRadius,
                Height = pointRadius,
                Margin = new Thickness(point.X - pointRadius / 2, point.Y - pointRadius / 2, 0, 0),
            };

            this.SetElementAlignment(element);

            return element;
        }

        /// <summary>
        /// Creates a UI element that represents a line.
        /// </summary>
        /// <param name="line">The line.</param>
        /// <returns>Line UI element.</returns>
        private FrameworkElement CreateElement(Line line)
        {
            return new System.Windows.Shapes.Line()
            {
                X1 = line.Start.X,
                Y1 = line.Start.Y,
                X2 = line.End.X,
                Y2 = line.End.Y,
            };
        }

        /// <summary>
        /// Sets the alignment of the given element to Left/Top.
        /// </summary>
        /// <param name="element">The element to change.</param>
        private void SetElementAlignment(FrameworkElement element)
        {
            element.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            element.VerticalAlignment = System.Windows.VerticalAlignment.Top;
        }

        /// <summary>
        /// Adds animation element for a point.
        /// </summary>
        /// <param name="point">The point.</param>
        private void AddAnimationElement(CGUtilities.Point point)
        {
            var element = this.CreateElement(point);
            this.AnimateAnimationElement(element, point);
            this.AddAnimationElement(element);
        }

        /// <summary>
        /// Adds animation element for a line.
        /// </summary>
        /// <param name="line">The line.</param>
        private void AddAnimationElement(Line line)
        {
            var element = this.CreateElement(line);
            this.AnimateAnimationElement(element, line);
            this.AddAnimationElement(element);
        }

        /// <summary>
        /// Sets the animation for the animated point element.
        /// </summary>
        /// <param name="element">The point element.</param>
        /// <param name="point">The point.</param>
        private void AnimateAnimationElement(FrameworkElement element, CGUtilities.Point point)
        {
            ScaleTransform scaleTransform = new ScaleTransform(1, 1);
            element.RenderTransform = scaleTransform;
            element.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);

            DoubleAnimation fadeIn = new DoubleAnimation(0, TimeSpan.FromMilliseconds(animationDuration))
            {
                DecelerationRatio = 1,
            };
            DoubleAnimation scaleAnimation = new DoubleAnimation(scaleAnimationScale, TimeSpan.FromMilliseconds(animationDuration))
            {
                AccelerationRatio = 1,
            };

            element.BeginAnimation(FrameworkElement.OpacityProperty, fadeIn);
            scaleTransform.BeginAnimation(ScaleTransform.ScaleXProperty, scaleAnimation);
            scaleTransform.BeginAnimation(ScaleTransform.ScaleYProperty, scaleAnimation);
        }

        /// <summary>
        /// Sets the animation for the animated line element.
        /// </summary>
        /// <param name="element">The line element.</param>
        /// <param name="line">The line.</param>
        private void AnimateAnimationElement(FrameworkElement element, Line line)
        {
            DoubleAnimation fadeIn = new DoubleAnimation(0, TimeSpan.FromMilliseconds(animationDuration))
            {
                DecelerationRatio = 1,
            };
            DoubleAnimation thicknessAnimation = new DoubleAnimation(1, 50, TimeSpan.FromMilliseconds(animationDuration))
            {
                AccelerationRatio = 1,
            };

            element.BeginAnimation(FrameworkElement.OpacityProperty, fadeIn);
            element.BeginAnimation(System.Windows.Shapes.Line.StrokeThicknessProperty, thicknessAnimation);
        }

        /// <summary>
        /// Adds the given animation element to the animation grid.
        /// </summary>
        /// <param name="element">The animation element.</param>
        private void AddAnimationElement(FrameworkElement element)
        {
            this.animationGrid.Children.Add(element);
            this.RemoveElementFromAnimationGrid(element);
        }

        /// <summary>
        /// Asynchronously removes the given animation element after the animation duration. 
        /// </summary>
        /// <param name="element"></param>
        async void RemoveElementFromAnimationGrid(FrameworkElement element)
        {
            await Task.Delay(animationDuration);
            this.animationGrid.Children.Remove(element);
        }

        /// <summary>
        /// Handles the left mouse button down event (dragging started).
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">Event arguments.</param>
        void mainGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.IsViewOnly)
            {
                return;
            }

            if (isDragging)
            {
                return;
            }

            this.isDragging = true;
            System.Windows.Point position = e.GetPosition(this);
            this.dragStartPoint = new CGUtilities.Point(position.X, position.Y);
            this.dragEndPoint = this.dragStartPoint;
            this.UpdateSelectionLine();
        }

        /// <summary>
        /// Handles the mouse move event (dragging).
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">Event arguments.</param>
        void mainGrid_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.IsViewOnly)
            {
                return;
            }

            if (this.isDragging)
            {
                this.selectionGrid.Visibility = System.Windows.Visibility.Visible;
                System.Windows.Point position = e.GetPosition(this);
                this.dragEndPoint = new CGUtilities.Point(position.X, position.Y);
                this.UpdateSelectionLine();
            }
        }

        /// <summary>
        /// Handles the left mouse button up event (dragging stopped).
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">Event arguments.</param>
        void mainGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.IsViewOnly)
            {
                return;
            }

            if (!this.isDragging)
            {
                return;
            }

            this.isDragging = false;
            this.selectionGrid.Visibility = System.Windows.Visibility.Collapsed;
            System.Windows.Point position = e.GetPosition(this);
            this.dragEndPoint = new CGUtilities.Point(position.X, position.Y);
            if (this.dragStartPoint.Equals(this.dragEndPoint))
            {
                this.Points.Add(this.dragStartPoint);
            }
            else
            {
                this.Lines.Add(new Line(this.dragStartPoint, this.dragEndPoint));
            }
        }

        /// <summary>
        /// Updates the start/end points of the selection line to match the current positions.
        /// </summary>
        private void UpdateSelectionLine()
        {
            this.selectionLine.X1 = this.dragStartPoint.X;
            this.selectionLine.Y1 = this.dragStartPoint.Y;
            this.selectionLine.X2 = this.dragEndPoint.X;
            this.selectionLine.Y2 = this.dragEndPoint.Y;
        }
    }
}
